#!/usr/bin/python
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

__version__   = '$Revision: 5 $'
__date__      = '$Date: 2017-12-20 15:08:00 (UTC+0200)$'
__copyright__ = 'Copyright (C) 2017 Avencall'
__author__    = 'jklingenmeyer@xivo.solutions'

import os
import sys
import time
import logging

VM_USER="asterisk"  #For future use
VM_GROUP="asterisk" #For future use too

VM_CONTEXT="voicemail-99"
VM_SPOOL="/var/spool/asterisk/voicemail"
VM_SPOOL_PATH=VM_SPOOL

SUFFIXES=("txt", "wav")
MAILBOX_TYPES=("Old", "INBOX")

#Init Logger
LOGFILE = '/var/log/vmpurge.log'
CN = 'vmpurge'
log = logging.getLogger()

class Syslogger(object):
    def write(self, data):
        global log
        log.error(data)

def init_logging(debug_mode=False):
    if debug_mode:
        log.setLevel(logging.DEBUG)
    else:
        log.setLevel(logging.INFO)
    logfilehandler = logging.FileHandler(LOGFILE)
    formatter = logging.Formatter('%%(asctime)s %s[%%(process)d] (%%(levelname)s) (%%(name)s): %%(message)s' % CN)
    logfilehandler.setFormatter(formatter)
    log.addHandler(logfilehandler)

    syslogger = Syslogger()
    sys.stderr = syslogger

# Convert file into list of mailboxes
# File format: list of elements, one per line
# Element format: context_name/vm_number
def get_mailboxes_from_file(filename):
    mailboxes = None
    try:
        with open(filename) as f:
            mailboxes = f.read().splitlines()
    except:
        log.exception('(vmpurge) Error while loading list of mailboxes from file %s. Exiting.' %(filename))
        sys.exit(2)
    return mailboxes

def get_subdirs(directory):
    return [name for name in os.listdir(directory)
            if os.path.isdir(os.path.join(directory, name))]

# Create list of mailboxes belonging to a particular context
# Element format: context_name/vm_number
def get_mailboxes_from_context(context):
    mailboxes = []
    rootdir = VM_SPOOL + "/" + context
    for vmdir in get_subdirs(rootdir):
        mb = context + "/" + vmdir
        mailboxes.append(mb)
        log.debug('(vmpurge) Get Mailbox - Adding %s' %(mb))
    return mailboxes

# Check for each mailbox if it is not broken
# A broken mailbox does not contain all the extensions for each voicemail
# E.g.: msg0001.txt exists but msg0001.wav is missing
def check_message_files(dir_path, file_list):
        log.debug('(vmpurge) Folder %s: ENTERING Check messages...' %(dir_path))
        mv_idx = {}
        for fn in file_list:
                pref,suf = os.path.splitext(fn)
                if mv_idx.has_key(pref):
                        mv_idx[pref] += 1
                else:
                        mv_idx[pref] = 1
        for key,val in mv_idx.items():
                if val != len(SUFFIXES):
                        log.error('(vmpurge) Folder %s: Broken mailbox - Only %d files starting with %s' %(dir_path,val,key))
        log.debug('(vmpurge) Folder %s: LEAVING Check messages.' %(dir_path))

# Rename all messages in dir
def rename_all_messages_in_dir(dir_path):
        msg_list = [fn for fn in os.listdir(dir_path) if os.path.isfile(os.path.join(dir_path,fn)) and fn.startswith('msg') and fn.endswith('.wav')]
        if msg_list and len(msg_list) > 0:
                msg_list.sort()
                for i,fn in enumerate(msg_list):
                        old_base = os.path.splitext(fn)[0]
                        txtfile = old_base + '.txt'
                        new_base = dir_path+'/msg' + str(i).zfill(4)
                        log.debug('(vmpurge) Folder %s: Renaming %s to %s' %(dir_path, old_base, new_base))
                        os.rename(dir_path + '/' + fn, new_base+'.wav')
                        os.rename(dir_path + '/' + txtfile, new_base+'.txt')
        else:
                log.debug('(vmpurge) Folder %s: Empty mailbox. Renaming not required.' %(dir_path))

# Check if file renaming is required for a mailbox. If so, execute the rename_first_message function
def renumber_files_in_dir(dir_path):
        msg_list = [fn for fn in os.listdir(dir_path) if os.path.isfile(os.path.join(dir_path,fn)) and fn.startswith('msg')]
        if msg_list and len(msg_list) > 0:
                msg_list.sort()
                if msg_list[0].startswith('msg0000'):
                        log.debug('(vmpurge) Folder %s: Zero message exists in this folder. Renaming not required.' %(dir_path))
                else:
                        log.debug('(vmpurge) Folder %s: Need to rename first message %s of this folder.' %(dir_path,msg_list[0]))
                        rename_first_message(dir_path,msg_list[0])
                check_message_files(dir_path, msg_list)
        else:
                log.debug('(vmpurge) Folder %s: Empty mailbox. Renaming not required.' %(dir_path))

# Rename the lowest number message with msg0000
# msg0000 files are required by Asterisk. Mailbox not usable if not present 
def rename_first_message(dir_path,filename):
        if filename.startswith('msg'):
                prefix, suffix = os.path.splitext(filename)
                for suff in SUFFIXES:
                        old = dir_path + '/' + prefix + '.' + suff
                        new = dir_path + '/msg0000.' + suff
                        if os.path.isfile(old):
                                os.rename(old, new)

# Remove older messages (starting with 'msg') than MAX_AGE days in dir_path folder
def delete_old_msg_in_dir(dir_path, max_days):    
        has_deleted = False
	now = time.time()
	for f in os.listdir(dir_path):
		full_f = os.path.join(dir_path, f)
		if os.stat(full_f).st_mtime < now - max_days * 86400:
                        log.debug('(vmpurge) Folder: %s - Current file %s is %s aged old' % (dir_path,f,os.stat(full_f).st_mtime))
			if os.path.isfile(full_f) and f.startswith('msg'):
				log.debug('(vmpurge) Folder: %s - Removing file %s' % (dir_path,f))
				os.remove(full_f)
                                has_deleted = True
        return has_deleted
		
def purge_vm(mv_list, max_days):
        res_nb = 0
	for mb in mv_list:
                res_nb = res_nb + 1
                log.info('(vmpurge) %s: ENTERING' %(mb))
                #mb = "default/7998"
		for mb_type in MAILBOX_TYPES:
                        log.debug('(vmpurge) %s: entering %s' %(mb,mb_type))
			path=VM_SPOOL_PATH + "/" + mb + "/" + mb_type
                        if os.path.isdir(path):
        			r = delete_old_msg_in_dir(path, max_days)
	        		if r:
		        		rename_all_messages_in_dir(path)
                        log.debug('(vmpurge) %s: leaving %s' %(mb,mb_type))
                log.info('(vmpurge) %s: LEAVING' %(mb))
        return res_nb

def main():
    init_logging(True)

    if (not sys.argv or len(sys.argv) < 3):
            log.error("(vmpurge) no args when calling script! Please call script with a filename and a nb of days. Exiting.")
            sys.exit(3)

    log.info('(vmpurge) application is launched')

    log.info('(vmpurge) retrieving mailboxes to purge')
    #mailboxes = get_mailboxes_from_file(sys.argv[1])
    mailboxes = get_mailboxes_from_context(sys.argv[1])
    
    max_days = int(sys.argv[2])

    log.info('(vmpurge) purging the mailboxes (messages older than %d days will be deleted)...' %(max_days))
    nb_vm = purge_vm(mailboxes,max_days)

    log.info('(vmpurge) ...done. Purge executed for %s mailboxes. Exiting.' %(nb_vm))
    sys.exit(0)

if __name__ == "__main__":
    main()
