#!/usr/bin/python
# -*- coding: utf-8 -*-
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

__version__   = '$Revision: 42 $'
__date__      = '$Date: 2018-10-05 10:43:38 (UTC+0200)$'
__copyright__ = 'Copyright (C) 2017-2018'
__author__    = 'jklingenmeyer@xivo.solutions'

# REQUIRED ARGUMENTS
# 1 - Context: Voicemail where the voicemail is. E.g.: default
# 2 - Mailbox: Mailbox number. E.g.: 7998
# OPTIONAL ARGUMENTS
# 3 - Path to the message file, without the extension.
#     E.g.: /var/spool/asterisk/voicemail/default/7998/INBOX/msg0003
# 4 - Nummsg:  Number of new messages in the mailbox. E.g.: 3

import re
import os
import sys
import time
import json
import codecs
import datetime
import smtplib
import logging
import requests
import subprocess
from logging.handlers import SysLogHandler
from email.MIMEMultipart import MIMEMultipart
from email.MIMEText import MIMEText
from logging.handlers import SysLogHandler
from ConfigParser import SafeConfigParser
from xivo_auth_client import Client as Auth
from xivo_confd_client import Client as Confd
from xivo.agi import AGI

agi = AGI()

#Init Logger
LOGFILE = '/var/log/sendmail.log'
CN = 'sendmail'
log = logging.getLogger()

#Global timeout and nb of attempts for commands such as rsync or sending email
#Will try NB_RETRIES times and wait for each TIMEOUT_SEC maximum
TIMEOUT_SEC = 15
NB_RETRIES = 3

#Information about SMTP server
SMTP_INFO = {
    'debug': False,
    'address': 'no-reply-sendmail-ag@company.fr',
    'alert_address': 'it_team@company.fr',
    'server' : '127.0.0.1',
    'port' : 25
}

#Information about destination server to push the message
#Will use info from /etc/ssh/ssh_config if not filled completely
RSYNC_INFO = {
    'debug': True,
    'public_url': 'server.company.fr',
    'user': '',
    'server': 'server',
    'dir': '/var/tmp/'
}

POST_INFO = {
    'debug': True,
    'public_url': 'server.company.fr',
    'server': 'http://publicserver.company.fr',
    'path': '/var/tmp/'
}

#Information about XiVO API auth
XIVO_INFO = {
    'user': 'sendmail',
    'pass': 'sendmail'
}

class Syslogger(object):
    def write(self, data):
        global log
        log.error(data)

def init_logging(debug_mode='DEBUG'):
    if debug_mode:
        log.setLevel(logging.DEBUG)
    else:
        log.setLevel(logging.INFO)
    logfilehandler = logging.FileHandler(LOGFILE)
    formatter = logging.Formatter('%%(asctime)s %s[%%(process)d] (%%(levelname)s) (%%(name)s): %%(message)s' % CN)
    logfilehandler.setFormatter(formatter)
    log.addHandler(logfilehandler)

    syslogger = Syslogger()
    sys.stderr = syslogger

#Make some cleanup on the callerid
def format_callerid(in_callerid):
    if(in_callerid=='"anonymous" <anonymous>'):
        in_callerid='anonymous'
    elif(in_callerid=='"Anonymous" <anonymous>'):
        in_callerid='anonymous'
 
    out_callerid_noquote = in_callerid.replace('"','') #delete "06XXXX" -> 06XXX
    out_callerid_nosup = out_callerid_noquote.replace('<','') # <06XX -> 06xx
    out_callerid_noinf = out_callerid_nosup.replace('>','') # <06XX -> 06xx
    out_callerid = out_callerid_noinf
    return out_callerid

#Get the voice mail infos 
#Find in the last file all the message details
def get_msg_infos(voicemail, vm_spool):
	if(os.path.isdir(vm_spool)):
	    os.chdir(vm_spool)
	    txtfilename = os.popen("/bin/ls -lRt *.txt | /usr/bin/head -1 | /usr/bin/awk '{print $9}'")
	    txtfile = txtfilename.readlines()[0].strip()
	    if not txtfile:
		txtfilename = os.popen("/bin/ls -lRt *.txt | /usr/bin/head -1 | /usr/bin/awk '{print $8}'")
		txtfile = txtfilename.readlines()[0].strip()
            res = txtfile
	else:
            res = None
	    log.error('(sendemail VID:%s) voicemail does not exist ' %(voicemail))
	return res

#Check voicemail lock
#Note: voicemail may be locked if it has been deleted in the web interface
def check_lock(voicemail, vm_spool):
    os.chdir(vm_spool)
    log.info("(sendemail VID:%s) change to dir %s" %(voicemail, vm_spool))
    if os.path.isfile("vm.lock"):
	log.info("(sendemail VID:%s) there is a lock" %(voicemail))
        os.remove("vm.lock")
        return 1
    else:
	log.info("(sendemail VID:%s) there is no lock" %(voicemail))
        return 0

# Find name and email from XiVO confd voicemail API
# If no email is defined in the advanced options, the send is stopped
def get_voicemail_info_api(voicemail):
	# Authentification XiVO REST API 1.1 + Token
	auth = Auth('127.0.0.1', username=XIVO_INFO['user'], password=XIVO_INFO['pass'], verify_certificate=False)
	token_data = auth.token.new('xivo_service', expiration=10)
	token = token_data['token']
	confd = Confd('127.0.0.1', port=9486, verify_certificate=False, token=token)
	res = confd.voicemails.list(search=voicemail)
	if res['total'] == 0:
                log.error("(sendemail VID:%s) no corresponding voicemail found ..." %(voicemail))
                return None
	elif res['total'] == 1 :
		#vm_email = res['items'][0]['email']
		#vm_email = res['items'][0]['options'][0][1]
                opt_list = voicemail,res['items'][0]['options'][0]
                log.debug('(sendemail VID:%s) Got %s from api request' %(voicemail,opt_list))
                vm_email = None
                if opt_list:
                        for i in opt_list[1]:
                                log.debug('(sendemail VID:%s) Got value %s from options' %(voicemail,i))
                                if "@" in i:
                                       vm_email = i
		vm_name = res['items'][0]['name']
		if vm_email is None or vm_email == '':
        	        log.info('(sendemail VID:%s) no address defined, the email will not be sent' %(voicemail))
                        return None
		else:
	                log.info('(sendemail VID:%s) email will be sent to  %s ...' %(voicemail,vm_email))
                if vm_name is None or vm_name == '':
                        log.info('(sendemail VID:%s) no fullname defined for this voicemail so the VM number will be used instead' %(voicemail))
                        vm_name = voicemail
                else:
                        log.info('(sendemail VID:%s) e-mail content will include VM fullname: %s ...' %(voicemail,vm_name))
	        return {'name'    : vm_name,
                        'email'   : vm_email}

#Prepare email notification to send
def set_email_content(voicemail, mailto, callerid, nummsg, dur, datetime, name, download_url):
    em_msg = MIMEMultipart()
    em_msg['Subject'] = "[%s] Nouveau message de %s" % (name,callerid)
    em_msg['From'] = SMTP_INFO['address']
    em_msg['To'] = mailto
    #You can add nummsg (total of new messages) in the content of the email if required.
    em_body = u"""\
        <html>
          <head>
            <style type="text/css" media="screen">
              body{
                  font-family: arial, sans-serif;
                  font-size: small;
                  color: #333;
              }
              table{
                  background-color: #dff0d8;
                  empty-cells:hide;
                  border: 0px;
                  width: 520px;
              }
              td{
                  background-color: white;
              }
              tr:hover {background-color: #f5f5f5}
              th, td {
                  padding: 7px;
              }
              th{
                  text-align:left;
                  color: #3c763d;
              }
              button {
                color: #ffffff;
                font-size: 15px;
                background: #006A53;
                padding: 10px 20px 10px 20px;
                text-decoration: none;
                display: inline-block;
                border: none;
                text-align: center;
                width: 100%% ;
                transition: all 0.5s;
                cursor: pointer;
                margin-right: 5px;
              }
              
              button a {
                cursor: pointer;
                display: inline-block;
                position: relative;
                transition: 0.5s;
              }
              
              button a:after {
                content: '\00bb';
                position: absolute;
                opacity: 0;
                top: 0;
                right: -20px;
                transition: 0.5s;
              }

              button:hover {
                background: #d95216;
                text-decoration: none;
              }

              button:hover a {
                padding-right: 25px;
              
              }
              
              button:hover a:after {
                opacity: 1;
                right: 0;
              }

              .headermevo {
                font-weight: normal;
                background: #006A53;
                color: white;
                height:50px;
                width:500px;
                border-color: #d6e9c6;
                padding: 7px 10px;
                border-bottom: 1px solid transparent;
                font-size: 20px;
                margin-top: 15px;
                margin-bottom: 10px;
              }
              .txtmevo {
                margin-left: 10px;
              }
              img{
                  float: left;
              }
            </style>
          </head>
          <body style="font-family: arial, sans-serif;font-size: small;color: #333">
<h2 style="font-weight: normal;background: #006A53; color: white;height:50px;width:500px;border-color: #d6e9c6;padding: 7px 10px;border-bottom: 1px solid transparent;font-size: 20px;margin-top: 15px;margin-bottom: 10px;"><span style="margin-left: 10px;">Message vocal pour <br /></span><span style="margin-left: 10px;"><b>%s</span></b></h2>
<table style="background-color: #dff0d8;empty-cells:hide;border: 0px;width: 520px;">
    <tr><th style="text-align:left;color: #3c763d;padding: 7px;">De</th><td style="background-color: white;padding: 7px;">%s</td></tr>
    <tr><th style="text-align:left;color: #3c763d;padding: 7px;">Date</th><td style="background-color: white;padding: 7px;">%s</td></tr>
    <tr><th style="text-align:left;color: #3c763d;padding: 7px;">Durée</th><td style="background-color: white;padding: 7px;">%s secondes</td></tr>
    <tr><th colspan="2" style="text-align:left;color: #3c763d;padding: 7px;"><a href="%s"><button type="button" style="color: #ffffff;font-size: 15px;background: #006A53;padding: 10px 20px 10px 20px;text-decoration: none;display: inline-block;border: none;text-align: center;width: 100%% ;transition: all 0.5s;cursor: pointer;margin-right: 5px;"><span style="cursor: pointer;display: inline-block;position: relative;transition: 0.5s;">CONSULTER LE MESSAGE VOCAL </span></button</a></th></tr>
</table>
    <p></p>    
    <p><br /><i>Ceci est un message automatique de XiVO, merci de ne pas y repondre.</i><br /></p>
          </body>
        </html>
    """  % (name, callerid, datetime.strftime("%d-%m-%Y, %H:%M:%S"), dur, download_url)
    em_msg.attach(MIMEText(em_body.encode('utf-8'), 'html', 'utf-8'))
    log.info("(sendemail VID:%s) body will be : %s" %(voicemail,em_body))       
    return em_msg

#Send an email if message posting failed
def send_email_alert(vmnb, filepath, json_infos):
    em_msg = MIMEMultipart()
    em_msg['Subject'] = "[ALERTE SENDMAIL] Message vocal non transmis - Agence %s" % (vmnb)
    em_msg['From'] = SMTP_INFO['address']
    em_msg['To'] = SMTP_INFO['alert_address']
    em_body = u"""\
        <html>
          <body style="font-family: arial, sans-serif;font-size: small">
    <p>ATTENTION : Le message vocal suivant de %s n'a pas été transmis au serveur distant.</p>
    <p>Il est toujours présent sur le XiVO et doit etre traité manuellement.</p>
    <p>Pour plus d'informations, merci de consulter les logs du script post_agi_sendmail.</p>
    <p>Fichier : %s<br /></p>
    <p>Le contenu JSON qui accompagnait le message était :<br />%s</p>
    <p><br /><i>Ceci est un message automatique de XiVO, merci de ne pas y répondre.</i><br /></p>
          </body>
        </html>
    """  % (vmnb,filepath,json_infos)
    em_msg.attach(MIMEText(em_body.encode('utf-8'), 'html', 'utf-8'))
    try:
        log.info("(sendemail VID:%s) try to send email ..." %(vmnb)) 
        conn_mail = smtplib.SMTP(SMTP_INFO['server'], SMTP_INFO['port'],timeout=TIMEOUT_SEC)
        conn_mail.set_debuglevel(SMTP_INFO['debug'])
        conn_mail.sendmail(SMTP_INFO['address'], SMTP_INFO['alert_address'], em_msg.as_string())
        log.info("(sendemail VID:%s) sent an alert email to %s about the post failure" %(vmnb,SMTP_INFO['alert_address'])) 
    except Exception as e:
        log.error("(sendemail VID:%s) e-mail was not sent to %s..."  %(vmnb,SMTP_INFO['alert_address']))
    finally:
        conn_mail.close()
        
#Send email notification
def send_email(voicemail, mailto, callerid, nummsg, dur, datetime, name, download_url):
    email_content = set_email_content(voicemail, mailto, callerid, nummsg, dur, datetime, name, download_url)
    res = 1
    cnt_tries = 0
    while (res != 0 and cnt_tries < NB_RETRIES):
        res = 0
        cnt_tries = cnt_tries + 1
        try:
            log.info("(sendemail VID:%s) try to send email ..." %(voicemail))       
            conn_mail = smtplib.SMTP(SMTP_INFO['server'], SMTP_INFO['port'],timeout=TIMEOUT_SEC)
            conn_mail.set_debuglevel(SMTP_INFO['debug'])
            try:
                conn_mail.sendmail(SMTP_INFO['address'], mailto, email_content.as_string())
            finally:
                conn_mail.close()
        except Exception as e:
            log.error("(sendemail VID:%s) e-mail was not sent to %s during attempt %s..."  %(voicemail,mailto,cnt_tries))
            res = 1
    return res

def convert_wav_to_mp3(wavfile,targetname):
        output="/var/tmp/%s" % (targetname)
        cmd='avconv -i %s -codec:a libmp3lame -b:a 320k %s' % (wavfile,output)
        subprocess.call(cmd, shell=True)
        return output

#Transfer the VM message into a remote directory using POST method request
def post_message(voicemail, srcfile, dstfile, payload):
        post_url = POST_INFO['server'] + '/save.php'
        #convert original file to mp3 in tmp folder
        tmp_file=convert_wav_to_mp3(srcfile,dstfile)
        #no need to remove srcfile because it is deleted by the cron daily purge
        #os.remove(srcfile)
	with open(tmp_file, 'rb') as f:
                cnt_tries = 0
                res = 0
	        files = {'file': f}
		#files = {
		#	 'json': (None, json.dumps(payload), 'application/json'),
		#	 'file': (os.path.basename(file), f, 'application/octet-stream')
		#}
                while (res != 200 and cnt_tries < NB_RETRIES):
                        cnt_tries = cnt_tries + 1
                        try:
                                r = requests.post(post_url, files=files, data=payload, timeout=TIMEOUT_SEC)
                                res = r.status_code
                        except requests.exceptions.ConnectionError:
                                log.warning("(sendemail) server refused connection")
                                res = 1
                        except requests.exceptions.Timeout:
                                log.error("(sendemail) timeout when connecting to the server")
                                res = 1
                        except:
                                log.error("(sendemail) unknown problem during connection to the server")
                                exc_type, exc_value, exc_traceback = sys.exc_info()
                                log.debug(repr(traceback.format_exception(exc_type, exc_value, exc_traceback)))
                                res = 1
                        if res != 200:
                                log.error('(sendemail VID:%s) upload attempt #%s failed: got error code %s from server.' % (voicemail,cnt_tries,res))
        if res != 200:
                log.exception('(sendemail VID:%s) was not able to post message to server after %s attempts. Sending email alert and exiting.' % (voicemail,NB_RETRIES))
                agi.set_variable("sendmail_error", "upload failed")
                send_email_alert(voicemail, tmp_file, payload)
                sys.exit(5)
        try:
                r_content = r.json()
        except ValueError:
                log.error("(sendemail) no JSON encoded response from server")
                log.error("(sendemail) Raw data: %s" %(r.text))
                r_content = None
        if r_content is None:
                log.exception('(sendemail VID:%s) got no download url from server. Exiting' % (voicemail))
                agi.set_variable("sendmail_error", "upload failed")
                send_email_alert(voicemail, tmp_file, payload)
                sys.exit(5)
        #dsturl = r_content['url']
        log.info('(sendemail VID:%s) message successfully uploaded to server. Got download url: %s' %(voicemail,r_content['url'])) 
        os.remove(tmp_file)
        return r_content
 
#Transfer the VM message into a remote directory using rsync system cmd
def rsync_message(voicemail, srcfile, dstfile):
        #convert original file to mp3 in tmp folder
        tmp_file=convert_wav_to_mp3(srcfile,dstfile)
        if RSYNC_INFO['user'] and RSYNC_INFO['user'] != '':
                target = "%(user)s@%(server)s:%(dir)s"  % (RSYNC_INFO)
        else:
                target = "%(server)s:%(dir)s"  % (RSYNC_INFO)
        dsturl="http://" + RSYNC_INFO['public_url'] + "/" + dstfile 
        cmd = ["rsync"]
        params = "-Xtp"
        params = params + "c" #Checksum of file
        params = params + "P" #Keep partially transferred file
        if RSYNC_INFO['debug']:
                cmd.append("--log-file=" + LOGFILE)
                params = params + "v" #Verbose for debug
        cmd.append("--timeout=" + str(TIMEOUT_SEC))
        cmd.append(params)
        cmd.append(tmp_file)
        cmd.append(target+dstfile)
        retcode = 1
        cnt_tries = 0
        while (retcode != 0 and cnt_tries < NB_RETRIES):
            retcode = 0
            cnt_tries = cnt_tries + 1
            try:
                    log.info('(sendemail VID:%s) trying to rsync file %s to %s with name %s' %(voicemail,srcfile,target,dstfile))
                    out = subprocess.check_output(cmd, stderr=subprocess.STDOUT, shell=False)
            except subprocess.CalledProcessError as e:
                    retcode = e.returncode
                    log.error('(sendemail VID:%s) error during rsync attempt. Exited with returnvalue %s and the following output: %s (cmd was %s)' % (voicemail,retcode,e.output,cmd))
            except OSError as e:
                    retcode = 1
                    log.error('(sendemail VID:%s) error during rsync attempt. Failed to execute command %s: %s' % (voicemail,cmd,str(e)))
        os.remove(tmp_file)
        if retcode != 0:
            log.exception('(sendemail VID:%s) too many failed attempts during rsync. Exiting.' % (voicemail))
            agi.set_variable("sendmail_error", "rsync failed")
            sys.exit(5)
        log.info('(sendemail VID:%s) rsync successfully done.' %(voicemail))
        return dsturl

def main():
    init_logging()
    log.info('(sendemail) application is launched')

    if (not sys.argv or len(sys.argv) < 3):
            log.info("(sendemail) ERROR: not enough args in calling script! Exiting.")

    #Information about the voicemail and his directory
    vm_context = sys.argv[1]
    vm_mailbox = sys.argv[2]
    if len(sys.argv) > 4:
        vm_nummsg = sys.argv[4]
    else:
        vm_nummsg = '0'
    if len(sys.argv) > 3:
        log.debug("(sendemail VID:%s) msg filename was given in args" %(vm_mailbox))
        vm_msgfile = sys.argv[3] # /var/spool/asterisk/voicemail/default/7998/INBOX/msg0003
        vm_spool = os.path.abspath(os.path.join(vm_msgfile, os.pardir)) #/var/spool/asterisk/voicemail/default/7998/INBOX
        vm_spool_root = os.path.abspath(os.path.join(vm_spool, os.pardir)) #/var/spool/asterisk/voicemail/default/7998
    else:
        log.debug("(sendemail VID:%s) no msg filename given in args, trying to find it by myself" %(vm_mailbox))
        vm_spool_root= "/var/spool/asterisk/voicemail/%s/%s" %(vm_context, vm_mailbox)
        vm_spool = "%s/INBOX" % vm_spool_root
    
    log.info("(sendemail VID:%s) context %s : mailbox %s : nummsg %s" %(vm_mailbox,vm_context,vm_mailbox,vm_nummsg))
    
    #Check if it's a consultation or not
    #If lock file vm.lock is present that means it is a consultation and the script is stopped
    if check_lock(vm_mailbox, vm_spool_root) != 0:
    	log.info("(sendemail VID:%s) notify is locked" %(vm_mailbox))
        agi.set_variable("sendmail_error", "voicemail is locked")
    	sys.exit(6)
    
    #Retrieve last wav and txt files
    if len(sys.argv) > 3:
        vm_txtfile = vm_msgfile + '.txt'
        if not os.path.isfile(vm_txtfile):
            agi.set_variable("sendmail_error", "no txt file found")
            sys.exit(1)
        vm_wavfile = vm_msgfile + '.wav'
    else:
        vm_txtfile = get_msg_infos(vm_mailbox,vm_spool)
        if vm_txtfile == None:
            agi.set_variable("sendmail_error", "no txt file found")
            sys.exit(1)
        vm_txtfile = vm_spool + '/' + vm_txtfile
        vm_wavfile = os.path.splitext(vm_txtfile)[0] + '.wav'
    if not os.path.isfile(vm_wavfile):
        agi.set_variable("sendmail_error", "no wave file found")
        sys.exit(1)
    log.info('(sendemail VID:%s) get the files : %s (and .wav)' %(vm_mailbox,vm_txtfile))
    msg_infos_config = SafeConfigParser()
    msg_infos_config.readfp(codecs.open(vm_txtfile, 'r', 'utf-8'))
    
    #Get infos from txt file
    vm_callerid = format_callerid(msg_infos_config.get('message', 'callerid'))
    vm_duration = msg_infos_config.get('message', 'duration')
    vm_origdate = datetime.datetime.fromtimestamp(int(msg_infos_config.get('message', 'origtime')))
    log.info('(sendemail VID:%s) callerid: %s, duration: %s,origtime: %s' %(vm_mailbox,vm_callerid,vm_duration,vm_origdate.strftime("%d%m%Y-%H:%M:%S")))
    
    #Find email dest info
    vm_infos = get_voicemail_info_api(vm_mailbox)
    if vm_infos is None: 
        agi.set_variable("sendmail_error", "not able to retrieve voicemail infos")
        sys.exit(3)

    #Prepare to upload message
    msg_infos = {'vm_mailbox': vm_mailbox, 'callerid': vm_callerid, 'duration': vm_duration, 'date' : vm_origdate.strftime("%d%m%Y-%H:%M:%S"), 
                 'vm_name': vm_infos['name'], 'email': vm_infos['email']} 
    connid = agi.get_variable("jlog8_ConnId")
    if connid is not None and connid != "":
        msg_infos['connid'] = connid
    msg_infos_j = {'data': json.dumps(msg_infos)}
    file_dest = vm_origdate.strftime("%Y%m%d-%H%M%S") + "-" + vm_mailbox + "-" + re.sub("[^0-9]", "", vm_callerid) + ".mp3"
    log.info('(sendemail VID:%s) destination filename will be: %s' %(vm_mailbox,file_dest))
   
    #Comment out to select the upload method:
    #- rsync message
    #url_dest = rsync_message(vm_mailbox, vm_wavfile, file_dest)
    #OR
    #- push message with POST method
    post_res = post_message(vm_mailbox, vm_wavfile, file_dest, msg_infos_j)
    url_dest = post_res['url']
    ret_id = post_res['id']
    agi.set_variable("jlog8_Integer1", vm_duration)
    agi.set_variable("jlog8_Integer2", ret_id)
    agi.set_variable("jlog8_String2", file_dest)
    agi.set_variable("jlog8_String7", vm_callerid.encode('utf-8'))
    agi.set_variable("jlog8_String8", vm_infos['name'])

    #Send email
    if send_email(vm_mailbox, vm_infos['email'], vm_callerid, vm_nummsg, vm_duration, vm_origdate, vm_infos['name'], url_dest) == 1:
        #Failure while sending email
        log.exception("(sendemail VID:%s) e-mail was not sent to %s after several attempts. Exiting."  %(vm_mailbox,vm_infos['email']))
        agi.set_variable("sendmail_error", "e-mail was not sent")
        sys.exit(7)
   
    log.info("(sendemail VID:%s) e-mail SEND REQUEST OK ..." %(vm_mailbox))
    
    log.info("(sendemail VID:%s) script processed successfully. Exiting." %(vm_mailbox))
    agi.set_variable("sendmail_error", "")
    sys.exit(0)

if __name__ == "__main__":
    main()
