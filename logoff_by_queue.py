#!/usr/bin/python
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

__version__   = '$Revision: 15 $'
__date__      = '$Date: 2018-09-19 11:33:51 (UTC+0200)$'
__copyright__ = 'Copyright (C) 2018'
__author__    = 'jklingenmeyer@xivo.solutions'

import os
import sys
import logging
import psycopg2
from time import sleep
from collections import defaultdict
from xivo_auth_client import Client as Auth
from xivo_agentd_client import Client as Agentd

#Init Logger
LOGFILE = '/var/log/autologoff.log'
CN = 'autologoff'
log = logging.getLogger()

API_INFO = {
    'user': 'sendmail',
    'pass': 'sendmail'
}

DB_INFO = {
    'dbname': 'asterisk',
    'user': 'asterisk',
    'password': 'pass'
}

class Syslogger(object):
    def write(self, data):
        global log
        log.error(data)

def init_logging(debug_mode=False):
    if debug_mode:
        log.setLevel(logging.DEBUG)
    else:
        log.setLevel(logging.INFO)
    logfilehandler = logging.FileHandler(LOGFILE)
    formatter = logging.Formatter('%%(asctime)s %s[%%(process)d] (%%(levelname)s) (%%(name)s): %%(message)s' % CN)
    logfilehandler.setFormatter(formatter)
    log.addHandler(logfilehandler)

    syslogger = Syslogger()
    sys.stderr = syslogger

def get_queues_from_file(filename):
    queues = None
    try:
        with open(filename) as f:
            lines = f.read().splitlines()
            queues = [q for q in lines if not q.startswith('#')]
    except:
        log.exception('(autologoff) Error while loading list of queues from file %s. Exiting.' %(file_list))
        sys.exit(2)
    return queues

def get_agent_ids(file_list):
    log.info('(autologoff) loading queue list from file %s' % (file_list))
    queues = get_queues_from_file(file_list)

    log.info('(autologoff) getting currently logged agents from DB')
    conn_string = "host='127.0.0.1' dbname='%(dbname)s' user='%(user)s' password='%(password)s'" % (DB_INFO)
    try:
        conn = psycopg2.connect(conn_string)
    except:
        log.exception('(autologoff) Error while connecting to DB. Exiting.')
        sys.exit(1)
    cur = conn.cursor()
    try:
        sql = 'SELECT distinct(agent_id) from agent_membership_status WHERE queue_name IN %s'
        cur.execute(sql, (tuple(queues),))
        agent_ids = defaultdict(list)
        agent_ids['logged'] = cur.fetchall()
        sql = 'SELECT distinct(userid) from queuemember WHERE queue_name IN %s'
        cur.execute(sql, (tuple(queues),))
        agent_ids['full'] = cur.fetchall()
        return agent_ids
    except:
        log.exception('(autologoff) Error while connecting to DB. Exiting.')
        sys.exit(1)
    finally:
        cur.close()

def logoff_agents_by_api(ids):
    res = 0
    auth = Auth('127.0.0.1', username=API_INFO['user'], password=API_INFO['pass'], verify_certificate=False)
    token_data = auth.token.new('xivo_service', expiration=10)
    token = token_data['token']
    c = Agentd('127.0.0.1', port=9493, verify_certificate=False, token=token)
    for agent in ids:
        agent_id = agent[0]
        log.info('            - logging out ID %s' % (agent_id))
        c.agents.logoff_agent(agent_id)
        res += 1
    return res

def generate_agentid_commands(d):
    l = list()
    for (uid,aid) in d.items():
        cmd = "devstate change Custom:*735%s***230***3%s INUSE" % (uid, aid)
        l.append(cmd)
        cmd = "devstate change Custom:*735%s***231***3%s INUSE" % (uid, aid)
        l.append(cmd)
        cmd = "devstate change Custom:*735%s***232***3%s NOT_INUSE" % (uid, aid)
        l.append(cmd)
    return l

def generate_agentnb_commands(d):
    l = list()
    for (uid,anb) in d.items():
        cmd = "devstate change Custom:*735%s***230*%s INUSE" % (uid, anb)
        l.append(cmd)
        cmd = "devstate change Custom:*735%s***231*%s INUSE" % (uid, anb)
        l.append(cmd)
        cmd = "devstate change Custom:*735%s***232*%s NOT_INUSE" % (uid, anb)
        l.append(cmd)
    return l

def execute_devstate_commands(cmds):
    #ast_socket = '/var/run/asterisk/asterisk.ctl'
    log.info('(autologoff) about to send the following commands to Asterisk :')
    for cmd in cmds:
        log.info('(autologoff)     - sending : %s' % (cmd))
        os.popen("asterisk -rx '%s'" % (cmd))
#        try:
#            s = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
#            s.settimeout(1)
#            s.connect(ast_socket)
#            log.info('(autologoff)     - sending : %s' % (cmd))
#            s.sendall(cmd)
#        except:
#            log.exception('(autologoff) Error while connecting to Asterisk socket. Exiting.')
#            sys.exit(1)
#        finally:
#            s.close()
        sleep(0.1)            
    log.info('(autologoff) commands sent (but cannot know if all of them were successful!)')

def change_agents_devstate(ids):
    log.info('(autologoff) trying to change dev state of the agents...')
    agent_nb_dict = dict()
    agent_id_dict = dict()
    log.info('(autologoff) getting user ids and agent numbers from DB')
    conn_string = "host='127.0.0.1' dbname='%(dbname)s' user='%(user)s' password='%(password)s'" % (DB_INFO)
    try:
        conn = psycopg2.connect(conn_string)
    except:
        log.exception('(autologoff) Error while connecting to DB. Exiting.')
        sys.exit(1)
    cur = conn.cursor()
    try:
        for a in ids:
                sql = 'select id from userfeatures where agentid = %s'
                cur.execute(sql, (a,))
                res = cur.fetchone()
                if res:
                    user_id = res[0]
                    agent_id_dict[user_id] = a[0]
                    sql = 'select number from agentfeatures where id = %s'
                    cur.execute(sql, (a,))
                    res = cur.fetchone()
                    agent_nb = res[0]
                    agent_nb_dict[user_id] = agent_nb
    except:
        log.exception('(autologoff) Error while connecting to DB. Exiting.')
        sys.exit(1)
    finally:
        cur.close()
    devstate_commands = generate_agentid_commands(agent_id_dict) + generate_agentnb_commands(agent_nb_dict)
    execute_devstate_commands(devstate_commands)

def main():
    init_logging()

    if (not sys.argv or len(sys.argv) < 3):
            log.error("(autologoff) no args when calling script! Please call script with a filename. Exiting.")
            sys.exit(3)

    log.info('(autologoff) application is launched')

    if sys.argv[1] == 'full':
        full = True
    else:
        full = False

    log.info('(autologoff) getting agents lists')
    agents = get_agent_ids(sys.argv[2])

    log.info('(autologoff) logging off the agents...')
    nb_agents = logoff_agents_by_api(agents['logged'])
    log.info('(autologoff) ...done. Logoff requested for %s agents.' %(nb_agents))
    
    if full:
        log.info('(autologoff) changing devstate of ALL of the agents...')
        change_agents_devstate(agents['full'])
    else:
        log.info('(autologoff) changing devstate of agents just being logged off...')
        change_agents_devstate(agents['logged'])
    log.info('(autologoff) ...done. Script is now exiting, bye.')
    
    sys.exit(0)

if __name__ == "__main__":
    main()

