# Asterisk and XiVO tools

## Script for automatically purge voice messages - vm_purge.py

### Description

* Deleting old voice messages
* Checking that at least one message is named "msg0000" (for Asterisk to avoid confusion). Making a renaming if necessary.
* Only "Old" and "INBOX" folders are taken into account by default.
* All actions are logged into a log file.

### Usage

**Two args:**

1. Path to the file containing the voicemails list to check
2. Age (in days) to consider messages as old (so to delete)

**Format of file to send as first parameter:**

1 voicemail per line, each line with format as follow: context/vm_number

```
default/7998
default/7999
```

## Script to disconnect agents based on the queues they belong to - logoff_by_queue.py

### Description

It logs off any agent logged in a queue given in the parameter file.
It uses both XiVO APIs and database queries to achieve this goal.

### Usage

```
./logoff_by_queue.py  full|logged queues_list.txt
```

**Argument:**

Two args are mandatory: 

* full|logged : if _full_, resetting BLF of all the devices belonging to the queue. If _logged_, resetting BLF of devices just being logged off by the script only (faster)
* full path to the file containing the list of queues

All the agents belonging to at least one of these queues will be logged off by the script.
The file must contain the name of the queue (system name, not the display name), one per line.

Example :

```
queuetest
queue2
queue3
```

**Additional info**

On top of the scipt some parameters can be edited, such as log file name, DB parameters, XiVO API access parameters.

**Cron job**
This script can be easily inserted in a cron job.

