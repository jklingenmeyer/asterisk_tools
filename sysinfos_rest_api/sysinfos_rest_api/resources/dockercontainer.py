# -*- coding: utf-8 -*-
# Copyright (C) 2018 Avencall
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

import json
import re
import io
import logging
import os.path
import datetime
import shutil
import requests

from docker import Client
from flask import request,jsonify
from flask_restful import Resource
from sysinfos_rest_api.core.exceptions import APIException
from sysinfos_rest_api.core.auth import AuthAdmin

VERSION = 1.0

logger = logging.getLogger(__name__)


class UnsupportedCommand(APIException):

    def __init__(self, command):
        super(UnsupportedCommand, self).__init__(
            status_code=503,
            message='Incompatible command',
            error_id='incompatible-command',
            details={
                'command': command
            }
        )

class DockerContainer(Resource):

    def __init__(self, **kwargs):
        self.method_decorators = [AuthAdmin.auth.login_required]

    def _make_error(self, status_code, sub_code, message):
        response = jsonify({
            'status': status_code,
            'message': sub_code,
            'details': message
        })
        response.status_code = status_code
        return response

    def get(self, command):
        try:
            args = json.loads(request.data) if request.data else {}
            command = command.lower()
            c = Client(base_url='unix://var/run/docker.sock')
            if command == 'containers':
                return c.containers(), 200
            elif command == 'images':
                return c.images(), 200
            elif command == 'infos':
                return c.info(), 200
            elif command == 'version':
                return c.version(), 200
            else:
                raise UnsupportedCommand(command)
        except APIException as e:
            return self._make_error(e.status_code, e.message, e.details)

