# -*- coding: utf-8 -*-
# Copyright (C) 2018 Avencall
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

import json
import re
import io
import logging
import os.path
import sys
import subprocess
import shlex
import requests

from flask import request,jsonify
from flask_restful import Resource
from sysinfos_rest_api.core.exceptions import APIException
from sysinfos_rest_api.core.auth import AuthAdmin

VERSION = 1.0

logger = logging.getLogger(__name__)


class UnsupportedCommand(APIException):

    def __init__(self, command):
        super(UnsupportedCommand, self).__init__(
            status_code=503,
            message='Incompatible command',
            details={
                'command': command
            }
        )

class RaidNotFound(APIException):

    def __init__(self, reason):
        super(RaidNotFound, self).__init__(
            status_code=503,
            message='Raid Not Found',
            details={
                'reason': reason
            }
        )

class CLICommandError(APIException):

    def __init__(self, error):
        super(CLICommandError, self).__init__(
            status_code=503,
            message='CLI Command Error',
            details={
                'reason': error
            }
        )

class Raid(Resource):

    def __init__(self, **kwargs):
        self.method_decorators = [AuthAdmin.auth.login_required]

    def _make_error(self, status_code, sub_code, message):
        response = jsonify({
            'status': status_code,
            'message': sub_code,
            'details': message
        })
        response.status_code = status_code
        return response

    def get(self, command):
        try:
            args = json.loads(request.data) if request.data else {}
            command = command.lower()
            if command == 'units':
                self._check_if_raid(args)
                return self._get_uinfos()
            elif command == 'ports':
                self._check_if_raid(args)
                return self._get_pinfos()
            else:
                raise UnsupportedCommand(command)
        except APIException as e:
            return self._make_error(e.status_code, e.message, e.details)

    def _check_if_raid(self, args):
        raid_cli = '/root/tw_cli'
        if not os.path.isfile(raid_cli):
            raise RaidNotFound('CLI Raid not found')
        else:
            p1 = subprocess.Popen(shlex.split('lspci'),stdout=subprocess.PIPE)
            p2 = subprocess.Popen(shlex.split('grep -i 3ware'), stdin=p1.stdout, stdout=subprocess.PIPE,stderr=subprocess.PIPE)
            p1.stdout.close()
            out, err = p2.communicate()
            if p2.returncode != 0:
                raise RaidNotFound('No 3ware RAID card found on the system')

    def _removeHeaders(self, output):
        output = output.split('\n')
        for index, line in enumerate(output):
            if line[0:4] == '----':
                output.pop(index)
                output.pop(index-1)
        return output
    
    def _parseCommand(self, param):
        cmdoutput = []
        p = subprocess.Popen(shlex.split('/root/tw_cli ' + param), stdout=subprocess.PIPE)
        out, err = p.communicate()
      
        if p.returncode == 0:
            return out
        else:
            raise CLICommandError(str(err)) 
        return cmdoutput
    
    def _get_uinfos(self):
        units = []
        i = 0
        output = self._removeHeaders(self._parseCommand('/c0/u0 show'))
        for line in output:
            if len(line.split()) > 7:
                i = i + 1
                name = line.split()[0] or 'unknown'
                utype = line.split()[1] or 'unknown'
                status = line.split()[2] or 'unknown'
                port = line.split()[5] or 'unknown'
                size = float(line.split()[7]) or 0
                l = {'name': name, 'type': utype, 'status': status, 'port': port, 'size_gb': size}
                units.append(l)
        res = {'total': i, 'units': units}
        return res, 200
        
    def _get_pinfos(self):
        ports = []
        i = 0
        for p in ['p0', 'p1']:
            output = self._removeHeaders(self._parseCommand('/c0/' + p + ' show'))
            for line in output:
                if len(line.split()) > 2:
                    name = line.split()[0] or 'unknown'
                    status = line.split()[1] or 'unknown'
                    unit = line.split()[2] or 'unknown'
                    size = float(line.split()[3]) or 0
                    blocks = int(line.split()[5]) or 0
                    serial = line.split()[6] or 'unknown'
                    l = {'name': name, 'status': status, 'unit': unit, 'size_gb': size, 'blocks': blocks, 'serial': serial}
                    i = i + 1
                    ports.append(l)
        res = {'total': i, 'ports': ports}
        return res, 200
