# -*- coding: utf-8 -*-
# Copyright (C) 2018 Avencall
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

import json
import re
import io
import logging
import os.path
import shutil
import requests

from flask import request,jsonify
from flask_restful import Resource
from xivo_auth_client import Client as Auth
from sysinfos_rest_api.core.exceptions import APIException
from sysinfos_rest_api.core.auth import AuthSuperAdmin


VERSION = 1.0

logger = logging.getLogger(__name__)


class UnsupportedCommand(APIException):

    def __init__(self, command):
        super(UnsupportedCommand, self).__init__(
            status_code=503,
            message='Incompatible command',
            details={
                'command': command
            }
        )

class HttpReqError(APIException):

    def __init__(self, command, output):
        super(HttpReqError, self).__init__(
            status_code=503,
            message='Request error',
            details={
                'command': command,
                'output': output
            }
        )

class MissingArgument(APIException):

    def __init__(self, data):
        super(MissingArgument, self).__init__(
            status_code=503,
            message='Missing Argument',
            details={
                'data_missing': data
            }
        )

class Originate(Resource):

    def __init__(self, **kwargs):
        self.user = kwargs['ws_user']
        self.passwd = kwargs['ws_passwd']
        self.ip = kwargs['ws_ip']
        self.method_decorators = [AuthSuperAdmin.auth.login_required]

    def _make_error(self, status_code, sub_code, message):
        response = jsonify({
            'status': status_code,
            'message': sub_code,
            'details': message
        })
        response.status_code = status_code
        return response

    def post(self, command):
        try:
            logger.info("data is %s" % (request.data))
            args = json.loads(request.data) if request.data else {}
            command = command.lower()
            if command == 'originate':
                #self._validate(args)
                return self._makecall(args)
            else:
                raise UnsupportedCommand(command)
        except APIException as e:
            return self._make_error(e.status_code, e.message, e.details)

    def _validate(self, args):
        required_params = ['Channel', 'Context', 'Exten', 'Priority']
        i=0
        for arg in args:
            if arg in required_params:
                i = i+1
        if i != len(required_params):
            raise MissingArgument('Required params are : %s' % (required_params))

    def _makecall(self, args):
        logger.info("args are : %s " % (args))
        auth = Auth('127.0.0.1', username=self.user, password=self.passwd, verify_certificate=False)
        token_data = auth.token.new('xivo_service', expiration=10)
        token = token_data['token']
        requests.packages.urllib3.disable_warnings()
        s = requests.Session()
        s.verify = False
        s.headers = {'Accept': 'application/json', 'Content-Type': 'application/json', 'X-Auth-Token': token}
        url= 'https://%s:9491/1.0/action/Originate' % (self.ip)
        try:
            response = s.post(url, data=json.dumps(args))
        except:
            raise HttpReqError('Originate','POST Error')
        if response.status_code != 200:
            raise HttpReqError('Originate','POST Error')
        else:
            data = {'status': 'ok'}
            return data, 200

