# -*- coding: utf-8 -*-
# Copyright (C) 2018 Avencall
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

import json
import re
import io
import apt
import apt_pkg
import logging
import os.path
import datetime
import platform
import psutil
import shutil
import requests
import subprocess

from flask import request,jsonify
from flask_restful import Resource
from sysinfos_rest_api.core.exceptions import APIException
from sysinfos_rest_api.core.auth import AuthAdmin

VERSION = 1.0

logger = logging.getLogger(__name__)


class UnsupportedCommand(APIException):

    def __init__(self, command):
        super(UnsupportedCommand, self).__init__(
            status_code=503,
            message='Incompatible command',
            details={
                'command': command
            }
        )

class SystemCommandError(APIException):

    def __init__(self, command, error):
        super(SystemCommandError, self).__init__(
            status_code=503,
            message='System command error',
            details={
                'command': command,
                'error': error
            }
        )


class System(Resource):

    def __init__(self, **kwargs):
        self.method_decorators = [AuthAdmin.auth.login_required]
    
    def _make_error(self, status_code, sub_code, message):
        response = jsonify({
            'status': status_code,
            'message': sub_code,
            'details': message
        })
        response.status_code = status_code
        return response

    def get(self, command):
        try:
            args = json.loads(request.data) if request.data else {}
            command = command.lower()
            if command == 'infos':
                return self._core_show_version(args)
            elif command == 'cpu':
                return self._core_show_hardware('cpu')
            elif command == 'memory':
                return self._core_show_hardware('mem')
            elif command == 'disk':
                return self._core_show_hardware('disk')
            elif command == 'updates':
                return self._core_get_security_patches(args)
            else:
                raise UnsupportedCommand(command)
        except APIException as e:
            return self._make_error(e.status_code, e.message, e.details)

    def _core_show_version(self, args):
        arch = platform.machine() #'x86_64'
        dist = platform.dist()
        kernel = platform.release()
        boot = datetime.datetime.fromtimestamp(psutil.boot_time()).strftime("%Y-%m-%d %H:%M:%S")
        data = {'architechture' : arch, 'distribution': dist, 'kernel': kernel, 'boottime': boot}
        return data, 200

    def _core_show_hardware(self, elem):
        if elem == 'cpu':
            nb_cpu = psutil.cpu_count()
            cpu = psutil.cpu_times_percent()
            data = {'total': nb_cpu, 'usage_percent': psutil.cpu_percent(interval=1)}
        elif elem == 'mem':
            mem = psutil.virtual_memory()
            data = {'total': mem.total, 'used': mem.used, 'free': mem.free}
        elif elem == 'disk':
            data = []
            i = 0
            for part in psutil.disk_partitions():
                usage = psutil.disk_usage(part.mountpoint)
                mp = { 'name': part.mountpoint,
                       'fstype': part.fstype,
                       'total': usage.total,
                       'used': usage.used,
                       'free': usage.free,
                       'usage_percent': usage.percent}
                data.append(mp)
                i = i + 1
            data = {'total': i, 'mountpoints': data}
        return data, 200

    def _is_sec_upgrade(self, pkg, archives, depcache):
        def is_sec_upgrade_helper(ver):
            for (f, index) in ver.file_list:
                if (f.archive in archives and f.origin == "Debian"):
                    return True
            return False
        inst_v = pkg.current_ver
        cand_v = depcache.get_candidate_ver(pkg)
        if is_sec_upgrade_helper(cand_v):
            return True
        for ver in pkg.version_list:
            if (inst_v and apt_pkg.version_compare(ver.ver_str, inst_v.ver_str) <= 0):
                continue
            if is_sec_upgrade_helper(ver):
                return True
        return False

    def _core_get_security_patches(self, args):
       p = subprocess.Popen(['apt-get','update'],stdout=subprocess.PIPE)
       o,e = p.communicate()
       dist_names = []
       dist_names.append('oldstable')
       dist_names.append('stable')
       dist_names.append(subprocess.check_output(["lsb_release", "-c", "-s"],universal_newlines=True).strip())
       archives = []
       for dist in dist_names:
           archives.append("%s-updates" % dist)
           archives.append(dist)
       pkgs = []
       apt_pkg.init()
       apt_pkg.config.set("Dir::Cache::pkgcache","")
       try:
           cache = apt_pkg.Cache(apt.progress.base.OpProgress())
       except SystemError as e:
           raise SystemCommandError('OpeningCache',e)
       depcache = apt_pkg.DepCache(cache)
       depcache.read_pinfile()
       depcache.init()
       try:
           depcache.upgrade(True)
           if depcache.del_count > 0:
               depcache.init()
           depcache.upgrade()
       except SystemError as e:
           raise SystemCommandError('MarkingUpgrade',e)
       for pkg in cache.packages:
           if not (depcache.marked_install(pkg) or depcache.marked_upgrade(pkg)):
               continue
           installed_ver = pkg.current_ver
           candidate_ver = depcache.get_candidate_ver(pkg)
           if candidate_ver == installed_ver:
               continue
           rec = {"name": pkg.name,
                  "security": self._is_sec_upgrade(pkg, archives, depcache),
                  "section": pkg.section,
                  "current_version": installed_ver.ver_str if installed_ver else '-',
                  "candidate_version": candidate_ver.ver_str if candidate_ver else '-',
                  "priority": candidate_ver.priority_str}
           pkgs.append(rec)
       security_updates = filter(lambda x: x.get('security'), pkgs)
       output = {'details': pkgs, 'total_security_updates': len(security_updates), 'total_update': len(pkgs)} 
       return output, 200
