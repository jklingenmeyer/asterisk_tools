# -*- coding: utf-8 -*-
# Copyright (C) 2018 Avencall
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

import json
import re
import io
import logging
import os.path
import shutil
import subprocess
import requests
import psycopg2

from flask import request,jsonify
from flask_restful import Resource
from xivo_auth_client import Client as Auth
from sysinfos_rest_api.core.exceptions import APIException
from sysinfos_rest_api.core.auth import AuthSuperAdmin


VERSION = 1.0

logger = logging.getLogger(__name__)


class UnsupportedCommand(APIException):

    def __init__(self, command):
        super(UnsupportedCommand, self).__init__(
            status_code=503,
            message='Incompatible command',
            details={
                'command': command
            }
        )

class SQLError(APIException):

    def __init__(self, request):
        super(SQLError, self).__init__(
            status_code=503,
            message='SQL Error',
            details={
                'request': request
            }
        )

class MissingArgument(APIException):

    def __init__(self, data):
        super(MissingArgument, self).__init__(
            status_code=503,
            message='Missing Argument',
            details={
                'data_missing': data
            }
        )

class XivoReqError(APIException):

    def __init__(self, device):
        super(XivoReqError, self).__init__(
            status_code=503,
            message='XiVO Request Error',
            details={
                'device': device
            }
        )

class Webrtc(Resource):

    def __init__(self, **kwargs):
        self.user = kwargs['ws_user']
        self.passwd = kwargs['ws_passwd']
        self.ip = kwargs['ws_ip']
        db_params = {'host': 'localhost',
                     'user': 'asterisk',
                     'password': 'proformatique',
                     'database': 'asterisk'}
        self.db_conn = psycopg2.connect(**db_params)
        self.method_decorators = [AuthSuperAdmin.auth.login_required]

    def _make_error(self, status_code, sub_code, message):
        response = jsonify({
            'status': status_code,
            'message': sub_code,
            'details': message
        })
        response.status_code = status_code
        return response

    def post(self, command):
        try:
            logger.info("data is %s" % (request.data))
            args = json.loads(request.data) if request.data else {}
            command = command.lower()
            if command == 'status':
                args['context'] = args.get('context', 'default')
                if args.get('number') is None:
                    raise MissingArgument('number')
                args['enabled'] = self._is_webrtc_enabled(args)
                return args, 200
            elif command in ['enable', 'disable']:
                args['context'] = args.get('context', 'default')
                if args.get('number') is None:
                    raise MissingArgument('number')
                if command == 'enable':
                    devid = self._get_deviceid_from_user(args)
                    if devid is not None and devid != '':
                        self._autoprov_and_sync_device(devid)
                    self._enable_webrtc(args)
                    return { "status": "ok", "previous_device": devid}, 200
                else:
                    self._disable_webrtc(args)
                    res = False
                    if args.get('device') is not None:
                        res = self._associate_device_to_user(args)
                    return { "status": "ok", "device_assiocated": res}, 200
            elif command == 'device':
                device = args.get('device')
                if device is None:
                    raise MissingArgument('device')
                return self._get_dev_infos(device)
            else:
                raise UnsupportedCommand(command)
        except APIException as e:
            return self._make_error(e.status_code, e.message, e.details)

    def _get_dev_infos(self, device):
        auth = Auth('127.0.0.1', username=self.user, password=self.passwd, verify_certificate=False)
        token_data = auth.token.new('xivo_service', expiration=180)
        token = token_data['token']
        requests.packages.urllib3.disable_warnings()
        s = requests.Session()
        s.verify = False
        s.headers = {'Accept': 'application/json', 'Content-Type': 'application/json', 'X-Auth-Token': token}
        url= 'https://%s:9486/1.1/devices/%s' % (self.ip,device)
        try:
            response = s.get(url)
        except:
            raise XivoReqError(device)
        if response.status_code != 200:
            raise XivoReqError(device)
        return response.json(), 200

    def _is_webrtc_enabled(self, user):
        cur = self.db_conn.cursor()
        try:
            cur.execute('select options from usersip where name = (select name from linefeatures where number=%s and context=%s)',
                        (user['number'], user['context']))
            res = cur.fetchone()
        except Exception:
            raise SQLError('SELECT options FROM usersip - number:%s - context:%s ' % (user['number'], user['context']))
        finally:
            cur.close()
        if res:
            for i in res[0]:
                if 'webrtc' in i and 'yes' in i:
                    return True
            return False
        else:
            return False

    def _get_deviceid_from_user(self, user):
        cur = self.db_conn.cursor()
        try:
            cur.execute('select device from linefeatures where number=%s and context=%s',
                        (user['number'], user['context']))
            res = cur.fetchone()
        except Exception:
            raise SQLError('SELECT device FROM linefeatures - number:%s - context:%s ' % (user['number'], user['context']))
        finally:
            cur.close()
        if res:
            return res[0]
        else:
            return None

    def _get_lineid_from_user(self, user):
        cur = self.db_conn.cursor()
        try:
            cur.execute('select id from linefeatures where number=%s and context=%s',
                        (user['number'], user['context']))
            res = cur.fetchone()
        except Exception:
            raise SQLError('SELECT id FROM linefeatures - number:%s - context:%s ' % (user['number'], user['context']))
        finally:
            cur.close()
        if res:
            return res[0]
        else:
            return None

    def _associate_device_to_user(self, args):
        lineid = self._get_lineid_from_user(args)
        if lineid is not None:
            auth = Auth('127.0.0.1', username=self.user, password=self.passwd, verify_certificate=False)
            token_data = auth.token.new('xivo_service', expiration=180)
            token = token_data['token']
            requests.packages.urllib3.disable_warnings()
            s = requests.Session()
            s.verify = False
            s.headers = {'Accept': 'application/json', 'Content-Type': 'application/json', 'X-Auth-Token': token}
            url= 'https://%s:9486/1.1/lines/%s/devices/%s' % (self.ip,lineid,args['device'])
            try:
                response = s.put(url)
            except:
                return False
            if response.status_code != 204:
                return False
            else:
                url = 'https://%s:9486/1.1/devices/%s/%s' % (self.ip,args['device'],'synchronize')
                response = s.get(url)
                logger.info("sync res is %s" % (response.status_code))
                return True

    def _autoprov_and_sync_device(self, device):
        actions = ['autoprov', 'synchronize']
        auth = Auth('127.0.0.1', username=self.user, password=self.passwd, verify_certificate=False)
        token_data = auth.token.new('xivo_service', expiration=180)
        token = token_data['token']
        requests.packages.urllib3.disable_warnings()
        s = requests.Session()
        s.verify = False
        s.headers = {'Accept': 'application/json', 'Content-Type': 'application/json', 'X-Auth-Token': token}
        for action in actions:
            url= 'https://%s:9486/1.1/devices/%s/%s' % (self.ip,device,action)
            try:
                response = s.get(url)
            except:
                raise XivoReqError(device)
            if response.status_code != 204:
                raise XivoReqError(device)
        
    def _enable_webrtc(self, user):
        self._update_webrtc_options(user, '{{webrtc,yes}}')

    def _disable_webrtc(self, user):
        self._update_webrtc_options(user, '{}')

    def _update_webrtc_options(self, user, options):
        cur = self.db_conn.cursor()
        try:
            logger.info('update usersip set options=%s where name = (select name from linefeatures where number=%s and context=%s)' % (options, user['number'], user['context']))
            cur.execute('update usersip set options=%s where name in (select name from linefeatures where number=%s and context=%s)',
                        (options, user['number'], user['context']))
            self.db_conn.commit()
        except Exception:
            raise SQLError('UPDATE usersip SET options - number:%s - context:%s - options: %s' % (user['number'], user['context'], options))
        finally:
            cur.close()
        self._asterisk_sip_reload()
        
    def _asterisk_sip_reload(self):
        command = 'sip reload'
        p = subprocess.Popen(['asterisk', '-rx', command],
                             stdout=subprocess.PIPE,
                             stderr=subprocess.STDOUT,
                             close_fds=True)
        output = p.communicate()[0]
        if p.returncode != 0:
            raise HttpReqError(command, output)
