# -*- coding: utf-8 -*-
# Copyright (C) 2018 Avencall
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

import json
import csv
import re
import io
import logging
import os.path
import shutil
import subprocess
import requests

from flask import request,jsonify
from flask_restful import Resource
from xivo_auth_client import Client as Auth
from sysinfos_rest_api.core.exceptions import APIException
from sysinfos_rest_api.core.auth import AuthAdmin


VERSION = 1.0

logger = logging.getLogger(__name__)


class UnsupportedCommand(APIException):

    def __init__(self, command):
        super(UnsupportedCommand, self).__init__(
            status_code=503,
            message='Incompatible command',
            details={
                'command': command
            }
        )

class HttpReqError(APIException):

    def __init__(self, command, output):
        super(HttpReqError, self).__init__(
            status_code=503,
            message='Request error',
            details={
                'command': command,
                'output': output
            }
        )

class Asterisk(Resource):

    def __init__(self, **kwargs):
        self.user = kwargs['ws_user']
        self.passwd = kwargs['ws_passwd']
        self.ip = kwargs['ws_ip']
        self.method_decorators = [AuthAdmin.auth.login_required]

    def _make_error(self, status_code, sub_code, message):
        response = jsonify({
            'status': status_code,
            'message': sub_code,
            'details': message
        })
        response.status_code = status_code
        return response

    def get(self, command):
        try:
            ami_cmd_list = {'core_show_channels': 'CoreShowChannels', 'sip_show_peers': 'SIPpeers', 'sip_peers_status': 'SIPPeerStatus', 'dahdi_show_channels': 'DAHDIShowChannels',
                            'device_state': 'DeviceStateList', 'presence_state': 'PresenceStateList', 'queues_status': 'QueueStatus', 'queues_summary': 'QueueSummary',
                            'agents_summary': 'Agents', 'pri_show_spans': 'PRIShowSpans', 'sip_show_registry': 'SIPShowRegistry'}
            args = json.loads(request.data) if request.data else {}
            command = command.lower()
            if command == 'core_show_version':
                return self._core_show_version(args)
            elif command == 'core_status':
                return self._core_status('CoreStatus', args)
            elif command == 'core_ping':
                return self._core_status('Ping', args)
            elif command in ami_cmd_list:
                return self._exec_ami_list_action(ami_cmd_list[command])
            else:
                raise UnsupportedCommand(command)
        except APIException as e:
            return self._make_error(e.status_code, e.message, e.details)

    def _core_show_version(self, args):
        #command = 'core show version'
        #res = self._exec_asterisk_command(command).split( )[1]
        #output = {'version' : res}
        data = self._exec_ami_action('CoreSettings', None)
        for e in data:
            version = e.pop('AsteriskVersion', None)
        if version:
            data = {'version' : version}
        else:
            data = {'version' : 'unknown'}
        return data, 200

    def _core_status(self, action, args):
        data = self._exec_ami_action(action, None)
        for e in data:
            e.pop('Response', None)
        data = [e for e in data if e]
        data = data[0]
        return data, 200

    def _exec_ami_list_action(self, action):
        #res = self._exec_ami_action(action,None)
        #output = res
        data = self._exec_ami_action(action,None)
        for e in data:
            e.pop('EventList', None)
            e.pop('Message', None)
            e.pop('Response', None)
            e.pop('Event', None)
            e.pop('Items', None)
            nb_it = e.pop('ListItems', None)
        data = [e for e in data if e]
        if(action == 'CoreShowChannels'):
            data = sorted(data, key=lambda k: k.get('BridgeId','-'))
        if nb_it:
            data = {'ListItems': nb_it, 'Items': data}

        return data, 200
    
    def _exec_ami_action(self, action, action_args):
        auth = Auth('127.0.0.1', username=self.user, password=self.passwd, verify_certificate=False)
        token_data = auth.token.new('xivo_service', expiration=10)
        token = token_data['token']
        requests.packages.urllib3.disable_warnings()
        s = requests.Session()
        s.verify = False
        s.headers = {'Accept': 'application/json', 'Content-Type': 'application/json', 'X-Auth-Token': token}
        url= 'https://%s:9491/1.0/action/%s' % (self.ip,action)
        try:
            response = s.post(url, data=action_args)
        except:
            raise HttpReqError(action,'POST Error')
        #try:
        #    response.raise_for_status()
        #except requests.exceptions.HTTPError as err:
        #    raise HttpReqError(action, 'raise for status Error')
        data = response.json()
        return data

    def _exec_asterisk_command(self, command):
        p = subprocess.Popen(['asterisk', '-rx', command],
                             stdout=subprocess.PIPE,
                             stderr=subprocess.STDOUT,
                             close_fds=True)
        output = p.communicate()[0]
        if p.returncode != 0:
            raise HttpReqError(command, output)

        return output

#    def _core_show_channels(self, args):
#        #command = 'core show channels'
#        #lines = self._exec_asterisk_command(command).splitlines()
#        #total_calls_nb = int(lines[-1].split( )[0])
#        #calls_nb = int(lines[-2].split( )[0])
#        #channels_nb = int(lines[-3].split( )[0])
#        #
#        #command = 'core show channels concise'
#        #lines = self._exec_asterisk_command(command).splitlines()
#        #fieldnames = ("Channel", "Context", "Exten", "Priority", "State", "Application", "Data", "CallerID", "Accountcode", "PeerAccount", "ColumnX", "Duration", "BridgeID", "UniqueID")
#        #csv_reader = csv.DictReader(lines, delimiter='!', fieldnames=fieldnames)
#        #calls_json = json.dumps([ row for row in csv_reader ], ensure_ascii=False)
#
#        #output = {'calls': json.loads(calls_json), 'active_calls' : calls_nb, 'active_channels' : channels_nb, 'calls_processed' : total_calls_nb}
#        action = 'CoreShowChannels'
#        res = self._exec_ami_action(action,None)
#        output = res
#        return output, 200
#    
#    def _dahdi_show_channels(self, args):
#        #command = 'dahdi show channels'
#        #lines = self._exec_asterisk_command(command).splitlines()
#        #lines.pop(0) #Removing header
#        ##lines = [" ".join(line.split()) for line in lines] #Removing multiple whitespaces
#        #lines = [re.sub(r'\s\s+','\t',line) for line in lines] #Removing multiple whitespaces
#        #fieldnames = ("Chan","Extension","Context","Language","MOH","Blocked","In Service","Description")
#        #csv_reader = csv.DictReader(lines, delimiter='\t', fieldnames=fieldnames)
#        #channels_json = json.dumps([ row for row in csv_reader ], ensure_ascii=False)
#
#        #output = {'channels': json.loads(channels_json)}
#        action = 'DAHDIShowChannels'
#        res = self._exec_ami_action(action,None)
#        output = res
#        return output, 200
    
