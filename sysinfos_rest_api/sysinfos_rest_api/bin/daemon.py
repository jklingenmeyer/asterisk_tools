# -*- coding: utf-8 -*-

# Copyright (C) 2018 Avencall
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

import logging

fromsysinfos_rest_api.core.daemonize import pidfile_context
fromsysinfos_rest_api.core.logging_helper import setup_logging
fromsysinfos_rest_api.controller import Controller

logger = logging.getLogger(__name__)

_DAEMONNAME = 'sysinfos_rest_api'
_DEFAULT_CONFIG = {
    'pidfile': '/var/run/{0}/{0}.pid'.format(_DAEMONNAME),
    'logfile': '/var/log/{}.log'.format(_DAEMONNAME),
    'rest_api': {'cors': {'enabled': True,
                          'allow_headers': 'Content-Type, X-Auth-Token'}}
}

def main():
    config = _DEFAULT_CONFIG

    setup_logging(config['logfile'], False, False)

    controller = Controller(config)

    with pidfile_context(config['pidfile'], False):
        controller.run()


if __name__ == '__main__':
    main()
