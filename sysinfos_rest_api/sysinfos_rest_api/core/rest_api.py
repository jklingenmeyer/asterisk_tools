# -*- coding: utf-8 -*-
# Copyright (C) 2018 Avencall
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

import cherrypy
import logging
import signal
import os

from cherrypy import wsgiserver
from datetime import timedelta
from flask import Flask, request, abort
from flask_cors import CORS
from flask_restful import Api, Resource
from sysinfos_rest_api.core import http_helpers, exceptions, config
from sysinfos_rest_api.core.config import AppConfig

VERSION = 1.0

app = Flask('sysinfos_rest_api')
logger = logging.getLogger(__name__)
api = Api(prefix='/{}'.format(VERSION))

def configure(global_config):

    http_helpers.add_logger(app, logger)
    #app.config.from_object('sysinfos_rest_api.core.config.AppConfig')
    app.config.from_pyfile('/etc/sysinfos_rest_api.cfg')
    app.after_request(http_helpers.log_request)
    app.before_request(check_remote_addr)
    app.secret_key = os.urandom(24)
    app.permanent_session_lifetime = timedelta(minutes=5)

    cors_config = dict(global_config['rest_api']['cors'])
    enabled = cors_config.pop('enabled', False)
    if enabled:
        CORS(app, **cors_config)

    load_resources(global_config)
    api.init_app(app)

def load_resources(global_config):
    #ws_config = app.config['XIVO_WS']
    ws_config = {'user': app.config['XIVO_WS_USER'], 'passwd': app.config['XIVO_WS_PASSWORD'], 'ip': app.config['XIVO_WS_HOST']}
    try:
        from sysinfos_rest_api.resources.asterisk import Asterisk
        api.add_resource(Asterisk, '/asterisk/<command>', 
            resource_class_kwargs={ 'ws_user': ws_config['user'], 'ws_passwd': ws_config['passwd'], 'ws_ip': ws_config['ip']})
    except ImportError:
        logger.warning("Asterisk resource will NOT be available")
    try:
        from sysinfos_rest_api.resources.xivo import Xivo
        api.add_resource(Xivo, '/xivo/<command>', 
            resource_class_kwargs={ 'ws_user': ws_config['user'], 'ws_passwd': ws_config['passwd'], 'ws_ip': ws_config['ip']})
    except ImportError:
        logger.warning("Asterisk resource will NOT be available")
    try:
        from sysinfos_rest_api.resources.originate import Originate
        api.add_resource(Originate, '/originate/<command>',
            resource_class_kwargs={ 'ws_user': ws_config['user'], 'ws_passwd': ws_config['passwd'], 'ws_ip': ws_config['ip']})
    except ImportError:
        logger.warning("Originate resource will NOT be available")
    try:
        from sysinfos_rest_api.resources.webrtc import Webrtc
        api.add_resource(Webrtc, '/webrtc/<command>', 
            resource_class_kwargs={ 'ws_user': ws_config['user'], 'ws_passwd': ws_config['passwd'], 'ws_ip': ws_config['ip']})
    except ImportError:
        logger.warning("WebRTC resource will NOT be available")
    try:
        from sysinfos_rest_api.resources.system import System
        api.add_resource(System, '/system/<command>')
    except ImportError:
        logger.warning("System resource will NOT be available")
    try:
        from sysinfos_rest_api.resources.raid import Raid
        api.add_resource(Raid, '/raid/<command>')
    except ImportError:
        logger.warning("Raid resource will NOT be available")
    try:
        from sysinfos_rest_api.resources.dockercontainer import DockerContainer
        api.add_resource(DockerContainer, '/docker/<command>')
    except ImportError:
        logger.warning("Docker resource will NOT be available")

def check_remote_addr():
    if request.remote_addr not in app.config['TRUST_IP_LIST']:
        abort(403)

def signal_handler(signum, frame):
    cherrypy.engine.exit()

def run(config):
    signal.signal(signal.SIGTERM, signal_handler)
    #http_config = app.config['REST_API']
    http_config = {'listen': app.config['REST_API_LISTEN_IP'], 'port': app.config['REST_API_LISTEN_PORT']}
    bind_addr = (http_config['listen'], http_config['port'])

    wsgi_app = wsgiserver.WSGIPathInfoDispatcher({'/': app})
    server = wsgiserver.CherryPyWSGIServer(bind_addr=bind_addr, wsgi_app=wsgi_app)

    logger.debug('HTTP server starting... uid: %s, listen: %s:%s', os.getuid(), bind_addr[0], bind_addr[1])

    try:
        server.start()
    except KeyboardInterrupt:
        server.stop()

