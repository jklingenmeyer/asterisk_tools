# -*- coding: utf-8 -*-
# Copyright (C) 2018 Avencall
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

import logging
from flask_httpauth import HTTPBasicAuth
from sysinfos_rest_api.core.config import AppConfig

class AuthAdmin:

    auth = HTTPBasicAuth()

    @auth.verify_password
    def verify(username, password):
        if not (username and password):
            return False
        return AppConfig.BASIC_AUTH.get(username) == password

class AuthSuperAdmin:

    auth = HTTPBasicAuth()

    @auth.verify_password
    def verify(username, password):
        if not (username and password):
            return False
        return AppConfig.BASIC_SUPER_AUTH.get(username) == password
