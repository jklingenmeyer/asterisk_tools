# -*- coding: utf-8 -*-

from StringIO import StringIO
import configparser

class AppConfig(object):

#    cfg_str = '[root]\n' + open('/etc/sysinfos_rest_api.cfg', 'r').read()
#    cfg_fp = StringIO(cfg_str)
#    config = RawConfigParser()
#    config.readfp(cfg_fp)

    with open('/etc/sysinfos_rest_api.cfg', 'r') as f:
        config_string = u'[main]\n' + f.read()
    config = configparser.ConfigParser()
    config.read_string(config_string)
    user = config['main']['BASIC_AUTH_USER'].encode('ascii','ignore').replace("'", "")
    password = config['main']['BASIC_AUTH_PASSWORD'].encode('ascii','ignore').replace("'", "")
    superuser = config['main']['BASIC_SUPER_AUTH_USER'].encode('ascii','ignore').replace("'", "")
    superpassword = config['main']['BASIC_SUPER_AUTH_PASSWORD'].encode('ascii','ignore').replace("'", "")
    BASIC_AUTH = {}
    BASIC_SUPER_AUTH = {}
    BASIC_AUTH[user] = password
    BASIC_SUPER_AUTH[superuser] = superpassword
