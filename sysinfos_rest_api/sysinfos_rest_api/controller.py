# -*- coding: utf-8 -*-

# Copyright (C) 2012-2015 Avencall
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

import logging

from sysinfos_rest_api.core import rest_api

logger = logging.getLogger(__name__)

class Controller(object):

    def __init__(self, config):
        self._config = config

    def run(self):
        self._run_rest_api()

    def _run_rest_api(self):
        rest_api.configure(self._config)
        rest_api.run(self._config['rest_api'])
