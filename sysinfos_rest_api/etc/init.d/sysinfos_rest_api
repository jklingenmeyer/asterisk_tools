#!/bin/sh
#
### BEGIN INIT INFO
# Provides:          sysinfos_rest_api
# Required-Start:    $local_fs $remote_fs $network $syslog
# Required-Stop:     $local_fs $remote_fs $network $syslog
# Should-Start:      asterisk postgresql
# Should-Stop:       asterisk postgresql
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: Startup daemon script for sysinfos_rest_api
### END INIT INFO
#
set -e

PATH=/sbin:/bin:/usr/sbin:/usr/bin
USER=root
GROUP=$USER
DAEMONNAME=sysinfos_rest_api
DAEMON=/usr/local/bin/$DAEMONNAME
PIDDIR=/var/run/$DAEMONNAME
PIDFILE=$PIDDIR/$DAEMONNAME.pid
SYSINFOS_REST_API_DEFAULT_FILE="/etc/default/sysinfos-rest-api"

. /lib/lsb/init-functions

test -x $DAEMON || exit 0

is_enabled() {
    # Check if startup variable is set to 1, if not we exit.
    if [ -f $SYSINFOS_REST_API_DEFAULT_FILE ]; then
        . $SYSINFOS_REST_API_DEFAULT_FILE
        if [ "$startup" != "yes" ]; then
           echo "${name} startup is disabled in $SYSINFOS_REST_API_DEFAULT_FILE"
           exit 0
        fi
    fi
}

for dir in "$PIDDIR" ; do
    [ -d "$dir" ] || install -d -o "$USER" -g "$GROUP" "$dir"
done

case "$1" in
  start)
    is_enabled
    log_daemon_msg "Starting" "$DAEMONNAME"

    if start-stop-daemon --start --quiet --oknodo --pidfile $PIDFILE --exec $DAEMON --user $USER;
    then
        log_end_msg 0
    else
        log_end_msg 1
    fi
    ;;
  stop)
    log_daemon_msg "Stopping" "$DAEMONNAME"
    if start-stop-daemon --stop --quiet --oknodo --retry 5 --pidfile $PIDFILE;
    then
        log_end_msg 0
    else
        log_end_msg 1
    fi
    rm -f $PIDFILE
    ;;
  force-reload|restart)
    $0 stop
    $0 start
    ;;
  status)
    status_of_proc -p $PIDFILE $DAEMON "$DAEMONNAME" && exit 0 || exit $?
    ;;
  *)
    echo "Usage: $0 {start|stop|restart|force-reload|status}"
    exit 1
    ;;
esac

exit 0
