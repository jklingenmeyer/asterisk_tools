#!/bin/bash

CALL_DURATION=7200 #7200s is 5h
LOG_VERBOSITY=5
LOG_FILE="/var/log/xivo-appels-fantomes.log"

lerr() { log 2 "ERROR: $1"; }
lwar() { log 3 "WARNING: $1"; }
linf() { log 4 "INFO: $1"; }
ldeb() { log 5 "DEBUG: $1"; }
log() {
    if [ $LOG_VERBOSITY -ge $1 ]; then
        datestr=`date +'%Y-%m-%d %H:%M:%S'`
        echo -e "[$datestr] $2" | fold -w150 -s | sed '2~1s/^/  /' >> ${LOG_FILE}
    fi
}

linf "Starting to check ghosts calls"

#AST_STATUS=$(systemctl status asterisk.service > /dev/null)
#GHOST_CALLS=`/usr/sbin/asterisk -rx 'core show channels concise'|awk -v d="$CALL_DURATION" -F'!' '$12 > d {print $1}'`
GHOST_CALLS=`systemctl -q is-active asterisk && /usr/sbin/asterisk -rx 'core show channels concise'|awk -v d="$CALL_DURATION" -F'!' '$12 > d {print $1}'`
NB_CALLS=0
for i in ${GHOST_CALLS}
do
    ldeb "Call hangup requested for channel ${i}..."
    /usr/sbin/asterisk -rx "channel request hangup ${i}" > /dev/null 2>&1
    ((NB_CALLS++))
done
if [ $NB_CALLS -eq 0 ]; then
    linf "No ghosts calls found, nothing to do"
else
    linf "Hangup requests done for $NB_CALLS call(s)"
fi

linf "Exiting script"


